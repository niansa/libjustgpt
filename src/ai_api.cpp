#define CPPHTTPLIB_OPENSSL_SUPPORT
#include "ai.hpp"
#include "json.hpp"
#include "http.hpp"

#include <string>
#include <string_view>



const std::string_view Ai::model_name = MODEL,
                       Ai::model_path = "/v1/engines/" MODEL "/completions";


Ai::Ai() {
    //
}
Ai::~Ai() {
    //
}

std::string Ai::complete(const std::string& prompt, char stop_token,
                         int top_k, float top_p, float temperature,
                         int max_output_len, int seed) {
    // Make request body
    std::string req_body_raw;
    {
        nlohmann::json req_body = {
            {"prompt", prompt},
            {"max_tokens", max_output_len},
            {"temperature", temperature},
            {"top_k", top_k},
            {"top_p", top_p}
        };
        if (stop_token) {
            std::string stop_token_str;
            stop_token_str.push_back(stop_token);
            req_body["stop"] = std::move(stop_token_str);
        }
        req_body_raw = req_body.dump();
    }
    // Make request
    httplib::Client client("https://api.textsynth.com");
    httplib::Headers headers;
    headers.emplace("Authorization", "Bearer "+std::string(TEXTSYNTH_API_TOKEN));
    auto result = client.Post(Ai::model_path.data(), headers, req_body_raw.c_str(), req_body_raw.size(), "application/json");
    // Check result
    if (result->status != 200) {
        throw error("API error code "+std::to_string(result->status));
    }
    // Parse result
    auto json_result = nlohmann::json::parse(result->body);
    return json_result["text"];
}
