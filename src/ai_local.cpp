#include "../ai/gpt2tc.h"
#include "ai.hpp"

#include <string>
#include <string_view>
#include <sstream>
#include <memory>
#include <cstring>
#include <sys/time.h>



const std::string_view Ai::model_name = MODEL,
                       Ai::model_path = MODEL_PATH;

Ai::Ai() {
    model = parse_model(model_name.data() + sizeof("gpt2_") - 1);
}
Ai::~Ai() {
    //
}

std::string Ai::complete(const std::string& prompt, char stop_token,
                         int top_k, float top_p, float temperature,
                         int max_output_len, int seed) {
    std::ostringstream fres;
    TextCompleteGlobalState *tcs;
    TextGenContext *ts;
    int count;
    struct timeval tv;
    std::unique_ptr<const char>(prompt1);
    struct list_head ts_list;

    tcs = text_complete_global_init(model, MODEL_PATH);

    if (seed == 0) {
        gettimeofday(&tv, NULL);
        seed = tv.tv_sec + tv.tv_usec;
    }

    prompt1.reset(trim_text(prompt.c_str()));
    if (prompt1.get()[0] == '\0') {
        prompt1.reset(strdup(" "));
    }

    ts = text_complete_start(tcs, prompt1.get(), top_k, top_p, temperature,
                             seed, max_output_len);
    count = 0;
    for(;;) {
        init_list_head(&ts_list);
        list_add_tail(&ts->link, &ts_list);
        text_complete_next(tcs, &ts_list);
        if (ts->out_text_len == 0)
            break;
        auto str = std::string_view{ts->out_text, static_cast<std::string_view::size_type>(ts->out_text_len)};
        // Callback and stop_token
        if (stop_token) {
            bool brk = false;
            for (const char character : str) {
                count++;
                if (character == stop_token) {
                    brk = true;
                    break;
                }
                fres << character;
            }
            if (brk) {
                break;
            }
        } else {
            fres << str;
        }
    }
    text_complete_end(ts);

    text_complete_global_end(tcs);

    return fres.str();
}
