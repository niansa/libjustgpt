#ifndef AI_HPP
#define AI_HPP
#include "../ai/gpt2tc.h"

#include <string>
#include <string_view>
#include <stdexcept>
#include <functional>



class Ai {
    GPT2ModelEnum model;

public:
    struct error : public std::runtime_error {
        using std::runtime_error::runtime_error;
    };

    const static std::string_view model_name,
                                  model_path;

    Ai();
    ~Ai();

    std::string complete(const std::string& prompt, char stop_token = 0,
                         int top_k = DEFAULT_TOP_K, float top_p = DEFAULT_TOP_P, float temperature = 1.0,
                         int max_output_len = MAX_OUTPUT_LEN, int seed = 0);
};

#endif // AI_HPP
